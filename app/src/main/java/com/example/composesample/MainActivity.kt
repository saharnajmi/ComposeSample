package com.example.composesample

import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.compose.foundation.ExperimentalFoundationApi
import androidx.compose.material.ExperimentalMaterialApi
import androidx.compose.material.Scaffold
import com.example.composesample.ui.Navigation
import com.example.composesample.ui.theme.ComposeSampleTheme


class MainActivity : ComponentActivity() {
    @ExperimentalMaterialApi
    @ExperimentalFoundationApi
    override fun onCreate(savedInstanceState: Bundle?) {

        super.onCreate(savedInstanceState)

        setContent {
            ComposeSampleTheme {
                Scaffold {
                    // HomeScreen()
                    // EditingScreen()
                    // AnimateColor()
                    Navigation()
                }
            }
        }
    }
}

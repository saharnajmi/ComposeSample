package com.example.composesample.ui

import androidx.compose.animation.animateColorAsState
import androidx.compose.animation.core.animateDpAsState
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.width
import androidx.compose.material.ExperimentalMaterialApi
import androidx.compose.material.Surface
import androidx.compose.runtime.Composable
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.unit.dp


@ExperimentalMaterialApi
@Composable
fun AnimateColor() {
    var surfaceColorState = remember {
        mutableStateOf(false)
    }

    var surfaceWidth = animateDpAsState(
        targetValue =
        if (surfaceColorState.value) 120.dp
        else 200.dp
    )

    val surfaceColor = animateColorAsState(
        targetValue = if (surfaceColorState.value) Color.Green else Color.Yellow
    )
    Column(
        modifier = Modifier.fillMaxSize(),
        content = {
            Surface(
                modifier = Modifier
                    .width(surfaceWidth.value)
                    .height(56.dp),
                content = {

                },
                color = surfaceColor.value,
                onClick = {
                    surfaceColorState.value = !surfaceColorState.value
                }
            )
        }
    )
}
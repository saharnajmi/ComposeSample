package com.example.composesample.ui

import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.material.Text
import androidx.compose.material.TextField
import androidx.compose.runtime.Composable
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.ui.Modifier
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import com.example.composesample.ui.theme.LightBlue


@Composable
fun EditingScreen() {
    Parent()
}

@Composable
fun Parent() {
    val text = remember { mutableStateOf("") }
    Column {
        ShowText(showText = text.value)
        ShowEditText(text = text.value, onValueTextChange = { text.value = it })
    }
}

@Composable
fun ShowText(showText: String) {
    Text(
        text = showText,
        fontSize = 30.sp,
        modifier = Modifier
            .fillMaxWidth()
            .height(
                200.dp
            )
            .background(LightBlue)
    )
}

@Composable
fun ShowEditText(text: String, onValueTextChange: (String) -> Unit) {
    TextField(
        value = text,
        onValueChange = {
            onValueTextChange(it)
        },
        label = { Text(text = "Enter your text") },
        modifier = Modifier
            .fillMaxWidth()
            .height(200.dp)
    )
}
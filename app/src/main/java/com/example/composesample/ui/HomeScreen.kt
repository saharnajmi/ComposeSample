package com.example.composesample.ui

import androidx.compose.foundation.ExperimentalFoundationApi
import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.lazy.GridCells
import androidx.compose.foundation.lazy.LazyVerticalGrid
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.Icon
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import com.example.composesample.R
import com.example.composesample.ui.theme.*

@ExperimentalFoundationApi
@Composable
fun HomeScreen() {
    Box(
        modifier = Modifier
            .background(DarkBlue)
            .fillMaxSize()
    ) {
        Column {
            GreetingSection()
            TitleCard()
            ShowWelcomeText(text = "Start a new journey")
            FeatureSection(
                items = listOf(
                    Item("Walking", R.drawable.ic_walk, LightYellow2),
                    Item("Cycling", R.drawable.ic_cycle, LightGreen1),
                    Item("Driving", R.drawable.ic_drive, LightBlue),
                    Item("Train", R.drawable.ic_train, LightYellow1),
                    Item("Flight", R.drawable.ic_flight, LightGreen2),
                    Item("Hiking", R.drawable.ic_hiking, LightMagenta)
                )
            )
        }
    }
}

@Composable
fun GreetingSection() {
    Row(
        horizontalArrangement = Arrangement.SpaceBetween,
        verticalAlignment = Alignment.CenterVertically,
        modifier = Modifier
            .fillMaxWidth()
            .padding(15.dp)
    ) {
        Column {
            Text(
                text = "Good Morning",
                color = TextWhite,
                fontSize = 15.sp
            )
            Text(
                text = "Sahar Najmi",
                color = TextWhite,
                fontWeight = FontWeight.Bold,
                fontSize = 25.sp
            )
        }
        Column(
            horizontalAlignment = Alignment.CenterHorizontally
        ) {
            Icon(
                painter = painterResource(id = R.drawable.ic_cloud),
                contentDescription = "search icon",
                tint = Color.White,
                modifier = Modifier.size(24.dp)
            )
            Text(
                text = "21 C",
                color = TextWhite,
                fontWeight = FontWeight.Bold
            )
            Text(
                text = "Cloudy",
                color = TextWhite,
                fontWeight = FontWeight.Thin
            )
        }
    }
}

@Composable
fun TitleCard(
) {
    Row(
        verticalAlignment = Alignment.CenterVertically,
        horizontalArrangement = Arrangement.SpaceBetween,
        modifier = Modifier
            .padding(15.dp)
            .clip(RoundedCornerShape(10.dp))
            .background(LightRed)
            .padding(horizontal = 15.dp, vertical = 20.dp)
            .fillMaxWidth()

    ) {
        Column {
            Text(
                text = "Set Your",
                color = TextWhite,
                fontSize = 30.sp
            )
            Text(
                text = "Workout Plan",
                color = TextWhite,
                fontSize = 30.sp,
            )
        }

        Image(
            painter = painterResource(id = R.drawable.running),
            contentDescription = "running",
            modifier = Modifier
                .size(70.dp)
        )
    }
}

@Composable
fun ShowWelcomeText(text: String) {
    Text(
        text = text,
        color = TextWhite,
        fontSize = 15.sp,
        modifier = Modifier.padding(20.dp)
    )
}

@ExperimentalFoundationApi
@Composable
fun FeatureSection(items: List<Item>) {
    LazyVerticalGrid(
        cells = GridCells.Fixed(2),
        contentPadding = PaddingValues(start = 7.5.dp, end = 7.5.dp, bottom = 100.dp),
        modifier = Modifier.fillMaxHeight()
    ) {
        items(items.size) {
            FeatureItem(items[it])
        }
    }
}

@Composable
fun FeatureItem(item: Item) {
    Box(
        modifier = Modifier
            .padding(8.dp)
            .aspectRatio(1f)
            .clip(RoundedCornerShape(10.dp))
            .background(item.backgroundColor)
            .padding(horizontal = 15.dp, vertical = 20.dp)
            .fillMaxWidth()

    ) {
        Text(
            text = item.title,
            color = DarkBlue,
            fontWeight = FontWeight.Bold,
            modifier = Modifier.align(Alignment.TopStart)
        )
        Text(
            text = "Start",
            color = TextWhite,
            fontSize = 14.sp,
            fontWeight = FontWeight.Bold,
            modifier = Modifier
                .clickable {
                    // item click
                }
                .align(Alignment.BottomEnd)
                .clip(RoundedCornerShape(10.dp))
                .background(LightRed)
                .padding(vertical = 5.dp, horizontal = 10.dp)
        )

        Icon(
            painter = painterResource(id = item.iconId),
            contentDescription = item.title,
            tint = Color.White,
            modifier = Modifier
                .align(Alignment.BottomStart)
                .size(32.dp)
        )
    }
}

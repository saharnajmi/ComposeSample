package com.example.composesample.ui

import androidx.compose.ui.graphics.Color
import androidx.annotation.DrawableRes

data class Item(
    val title: String,
    @DrawableRes val iconId: Int,
    val backgroundColor: Color
)
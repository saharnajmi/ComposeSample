package com.example.composesample.ui

sealed class Screen(val rote: String) {
    object MainScreen : Screen("main_screen")
    object DetailScreen : Screen("detail_screen")

    fun withArgs(vararg args: String): String {
        return buildString {
            append(rote)
            args.forEach { arg ->
                append("/$arg")
            }
        }
    }
}

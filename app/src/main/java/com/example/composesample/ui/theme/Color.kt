package com.example.composesample.ui.theme

import androidx.compose.ui.graphics.Color

val Purple200 = Color(0xFFBB86FC)
val Purple500 = Color(0xFF6200EE)
val Purple700 = Color(0xFF3700B3)
val Teal200 = Color(0xFF03DAC5)

val TextWhite = Color(0xffeeeeee)
val DarkBlue = Color(0xff06164c)
val LightRed = Color(0xfffc879a)

val LightBlue = Color(0xFFB2DFFF)
val LightYellow1 = Color(0xFFFFFA99)
val LightYellow2 = Color(0xFFFFF877)
val LightGreen1 = Color(0xff54e1b6)
val LightGreen2 = Color(0xFFD9FDB5)
val LightMagenta = Color(0xFFEBC3F1)
